# Subtitle Importer for Unity 2017.1 and newer

Parses `.SRT` files and saves them as a `ScriptableObject` (`SubtitleSequence`). This simplifies the workflow of pulling in subtitles from external sources and provides parse errors on import instead of at runtime.

## Usage
See `Assets/Subtitles/Sample/PlayAudioWithSubs.cs` for usage.