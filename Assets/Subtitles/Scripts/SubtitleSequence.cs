namespace Silvr.Subtitles
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using UnityEngine;

    [Serializable]
    [CreateAssetMenu]
    public class SubtitleSequence : ScriptableObject
    {
        [SerializeField]
        protected SubtitleEntry[] entries;

        public ReadOnlyCollection<SubtitleEntry> Entries
        {
            get { return this.entries.ToList().AsReadOnly(); }
        }

        public static SubtitleSequence Create(IEnumerable<SubtitleEntry> entries)
        {
            var instance = ScriptableObject.CreateInstance<SubtitleSequence>();
            instance.entries = entries.ToArray();

            return instance;
        }

        public Queue<SubtitleEntry> AsQueue()
        {
            return new Queue<SubtitleEntry>(this.entries);
        }

#if UNITY_EDITOR
        void OnValidate()
        {
            this.entries = this.entries.OrderBy(e => e.StartTime).ToArray();
        }
#endif
    }
}