﻿namespace Silvr.Subtitles
{
    using System;
    using UnityEngine;

    [Serializable]
    public class SubtitleEntry
    {
        [SerializeField]
        private float startTime;
        [SerializeField]
        private float endTime;
        [SerializeField]
        private string text;

        public float StartTime
        {
            get { return this.startTime; }
        }

        public float EndTime
        {
            get { return this.endTime; }
        }

        public string Text
        {
            get { return this.text; }
        }

        public SubtitleEntry(string text, float startTime, float endTime)
        {
            this.startTime = startTime;
            this.endTime = endTime;
            this.text = text;
        }
    }
}