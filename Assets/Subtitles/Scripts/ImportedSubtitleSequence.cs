namespace Silvr.Subtitles
{
    using System;
    using System.Linq;
    using System.Collections.Generic;
    using UnityEngine;

    [Serializable]
    public class ImportedSubtitleSequence : SubtitleSequence
    {
        [Serializable]
        public class ImportError
        {
            public string Message;
            public string FilePath;
            public int Line;

            public ImportError(string message, string filePath, int line = 0)
            {
                this.Message = message;
                this.FilePath = filePath;
                this.Line = line;
            }
        }

        [SerializeField]
        private ImportError[] importErrors;

        public ImportError[] ImportErrors
        {
            get { return this.importErrors; }
        }

        public static ImportedSubtitleSequence CreateImported(IEnumerable<SubtitleEntry> entries, IEnumerable<ImportError> importErrors = null)
        {
            var instance = ImportedSubtitleSequence.CreateImported(entries);
            instance.importErrors = importErrors.ToArray();

            return instance;
        }

        public static ImportedSubtitleSequence CreateImported(IEnumerable<SubtitleEntry> entries)
        {
            var instance = ScriptableObject.CreateInstance<ImportedSubtitleSequence>();
            instance.entries = entries.ToArray();

            return instance;
        }
    }
}