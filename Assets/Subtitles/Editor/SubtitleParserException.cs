﻿namespace Silvr.Subtitles
{
    using System;

    public class SubtitleParserException : FormatException
    {
        public int Line { get; private set; }

        public SubtitleParserException(string message, int line) : base(message)
        {
            this.Line = line;
        }
    }
}