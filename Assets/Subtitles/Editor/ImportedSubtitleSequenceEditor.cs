namespace Silvr.Subtitles
{
    using System.Globalization;
    using UnityEditor;
    using UnityEngine;

    [CustomEditor(typeof(ImportedSubtitleSequence))]
    public class ImportedSubtitleSequenceEditor : SubtitleSequenceEditor
    {
        public override void OnInspectorGUI()
        {
            this.DrawImportErrors();
            GUILayout.Space(5f);

            EditorGUI.BeginDisabledGroup(true);
            base.OnInspectorGUI();
            EditorGUI.EndDisabledGroup();
        }

        private void DrawImportErrors()
        {
            var importedSequence = this.target as ImportedSubtitleSequence;
            var errors = importedSequence.ImportErrors;
            var currentEvent = Event.current;

            if (errors.Length == 0)
                return;

            GUILayout.Space(5f);
            GUILayout.Label(string.Format("Errors ({0}):", errors.Length), EditorStyles.boldLabel);

            GUILayout.BeginVertical(EditorStyles.helpBox, GUILayout.MinHeight(150f));

            for (int i = 0; i < errors.Length; i++)
            {
                GUILayout.BeginHorizontal((i & 1) == 0 ? Styles.evenBackground : Styles.oddBackground,
                    GUILayout.Height(EditorGUIUtility.singleLineHeight));
                {
                    GUILayout.Label(new GUIContent(errors[i].Message, Styles.errorIcon),
                        Styles.messageStyle, GUILayout.ExpandWidth(true));

                    string location = string.Format("{0}:{1}",
                        System.IO.Path.GetFileName(errors[i].FilePath),
                        errors[i].Line.ToString(CultureInfo.InvariantCulture));
                    GUILayout.Label(location, EditorStyles.miniLabel, GUILayout.ExpandWidth(false));
                }
                GUILayout.EndHorizontal();

                var controlRect = GUILayoutUtility.GetLastRect();

                // Handle double clicked
                if (currentEvent.type == EventType.MouseDown && currentEvent.button == 0 &&
                    controlRect.Contains(currentEvent.mousePosition))
                {
                    if (currentEvent.clickCount == 2)
                    {
                        UnityEngine.Object @object = (!string.IsNullOrEmpty(errors[i].FilePath)) ?
                            AssetDatabase.LoadMainAssetAtPath(errors[i].FilePath) : null;

                        AssetDatabase.OpenAsset(@object ?? importedSequence, errors[i].Line);
                        GUIUtility.ExitGUI();
                    }
                    currentEvent.Use();
                }
            }

            GUILayout.EndVertical();
        }

        internal class Styles
        {
            public static GUIStyle evenBackground;
            public static GUIStyle oddBackground;
            public static GUIStyle messageStyle = "CN StatusInfo";
            public static Texture2D errorIcon = LoadIcon("console.erroricon.sml");

            static Styles()
            {
                GUIStyle unityEvenBackground = "CN EntryBackEven";

                oddBackground = new GUIStyle(GUIStyle.none);
                evenBackground = new GUIStyle(GUIStyle.none);
                evenBackground.normal = unityEvenBackground.normal;
            }

            private static Texture2D LoadIcon(string name)
            {
                var method = typeof(EditorGUIUtility).GetMethod("LoadIcon",
                    System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Static);
                return method.Invoke(null, new object[] { name }) as Texture2D;
            }

        }
    }
}