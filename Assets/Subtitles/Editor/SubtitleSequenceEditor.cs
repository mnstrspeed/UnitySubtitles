namespace Silvr.Subtitles
{
    using UnityEngine;
    using UnityEditor;

    [CustomEditor(typeof(SubtitleSequence))]
    public class SubtitleSequenceEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            this.serializedObject.Update();

            var entries = this.serializedObject.FindProperty("entries");
            for (int i = 0; i < entries.arraySize; i++)
            {
                EditorGUILayout.BeginHorizontal();
                {
                    EditorGUILayout.PropertyField(entries.GetArrayElementAtIndex(i));

                    Rect entryRect = GUILayoutUtility.GetLastRect();
                    Rect rightButtonRect = new Rect(entryRect.x + entryRect.width - 20f, entryRect.y, 20f, EditorGUIUtility.singleLineHeight);

                    if (GUI.Button(rightButtonRect, "-", EditorStyles.miniButton))
                    {
                        entries.DeleteArrayElementAtIndex(i);
                    }
                }
                EditorGUILayout.EndHorizontal();
                EditorGUILayout.Space();
            }

            if (GUILayout.Button("+"))
            {
                int i = entries.arraySize;
                entries.InsertArrayElementAtIndex(i);

                var entry = entries.GetArrayElementAtIndex(i);
                entry.FindPropertyRelative("startTime").floatValue = i > 0 ?
                    entries.GetArrayElementAtIndex(i - 1).FindPropertyRelative("endTime").floatValue : 0f;
                entry.FindPropertyRelative("endTime").floatValue = entry.FindPropertyRelative("startTime").floatValue + 1f;
            }

            this.serializedObject.ApplyModifiedProperties();
        }
    }
}