namespace Silvr.Subtitles
{
    using UnityEngine;
    using UnityEditor;

    [CustomPropertyDrawer(typeof(SubtitleEntry))]
    public class SubtitleEntryDrawer : PropertyDrawer
    {
        public static readonly int NumTextLines = 2;
        public static readonly float PropertyHeight = (1 + NumTextLines) * EditorGUIUtility.singleLineHeight + NumTextLines * 2f;

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return PropertyHeight;
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, property);

            Rect firstLineRect = new Rect(position.x, position.y, position.width, EditorGUIUtility.singleLineHeight);
            Rect textRect = new Rect(firstLineRect.x, firstLineRect.y + firstLineRect.height + 2f, firstLineRect.width, position.height - firstLineRect.height);

            Rect timeRect = firstLineRect;
            Rect beginTimeRect = new Rect(timeRect.x, timeRect.y, 100f, timeRect.height);
            Rect endTimeRect = new Rect(timeRect.x + beginTimeRect.width, timeRect.y, beginTimeRect.width, timeRect.height);

            float labelWidth = 40f;
            EditorGUI.LabelField(beginTimeRect, "Start");
            beginTimeRect.x += labelWidth;
            beginTimeRect.width -= labelWidth + 4f;
            EditorGUI.PropertyField(beginTimeRect, property.FindPropertyRelative("startTime"), GUIContent.none);

            labelWidth = 30f;
            EditorGUI.LabelField(endTimeRect, "End");
            endTimeRect.x += labelWidth;
            endTimeRect.width -= labelWidth + 4f;
            EditorGUI.PropertyField(endTimeRect, property.FindPropertyRelative("endTime"), GUIContent.none);

            var textProperty = property.FindPropertyRelative("text");
            textProperty.stringValue = EditorGUI.TextArea(textRect, textProperty.stringValue);

            EditorGUI.EndProperty();
        }
    }
}