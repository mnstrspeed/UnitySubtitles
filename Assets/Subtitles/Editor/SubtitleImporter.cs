﻿namespace Silvr.Subtitles
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using UnityEditor.Experimental.AssetImporters;

    [ScriptedImporter(1, "srt")]
    public class SubtitleImporter : ScriptedImporter
    {
        public override void OnImportAsset(AssetImportContext ctx)
        {
            string fileName = Path.GetFileNameWithoutExtension(ctx.assetPath);
            string rawInput = File.ReadAllText(ctx.assetPath);

            // Parse subtitle file
            var subtitleEntries = new List<SubtitleEntry>();
            var errors = new List<ImportedSubtitleSequence.ImportError>();
            try
            {
                subtitleEntries.AddRange(SubtitleParser.ParseEntries(rawInput));
            }
            catch (SubtitleParserException e)
            {
                errors.Add(new ImportedSubtitleSequence.ImportError(e.Message, ctx.assetPath, e.Line));
            }
            catch (Exception e)
            {
                errors.Add(new ImportedSubtitleSequence.ImportError(e.Message, ctx.assetPath));
            }

            var subtitleSequence = ImportedSubtitleSequence.CreateImported(subtitleEntries, errors);
            ctx.SetMainAsset(fileName, subtitleSequence);
        }
    }
}