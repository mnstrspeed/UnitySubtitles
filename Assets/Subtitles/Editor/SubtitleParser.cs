﻿namespace Silvr.Subtitles
{
    using System;
    using System.Collections.Generic;

    public static class SubtitleParser
    {
        public static Queue<SubtitleEntry> ParseEntries(string raw)
        {
            var entries = new Queue<SubtitleEntry>();
            var lines = raw.Split(new string[] { "\r\n", "\r", "\n" }, StringSplitOptions.None);

            int lineIndex = 0;
            for (; lineIndex + 2 < lines.Length; lineIndex++)
            {
                // Skip subtitle index
                lineIndex++;
                // Read time string
                string timeString = lines[lineIndex++].Trim();
                // Read text until we encounter an empty line
                var textLines = new List<string>();
                while (lineIndex < lines.Length && !string.IsNullOrEmpty(lines[lineIndex].Trim()))
                    textLines.Add(lines[lineIndex++].Trim());
                string text = string.Join("\n", textLines.ToArray());

                // Extract start and end time from timeString
                string[] timeStringParts = timeString.Split(
                    new string[] { " --> " }, StringSplitOptions.None);

                if (timeStringParts.Length != 2)
                    throw new SubtitleParserException("Invalid time string format", lineIndex + 1);

                TimeSpan startTime = ParseTime(timeStringParts[0]);
                TimeSpan endTime = ParseTime(timeStringParts[1]);

                entries.Enqueue(new SubtitleEntry(text,
                    (float)startTime.TotalSeconds, (float)endTime.TotalSeconds));
            }

            // Make sure there is no unparsed content left
            for (; lineIndex < lines.Length; lineIndex++)
            {
                if (!string.IsNullOrEmpty(lines[lineIndex]))
                    throw new SubtitleParserException("Incomplete subtitle entry", lineIndex + 1);
            }

            return entries;
        }

        private static TimeSpan ParseTime(string raw)
        {
            return TimeSpan.Parse(raw.Replace(',', '.'));
        }
    }
}