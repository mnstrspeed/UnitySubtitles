﻿using Silvr.Subtitles;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(AudioSource))]
public class PlayAudioWithSubs : MonoBehaviour
{
    public AudioClip IntroAudioClip;
    public SubtitleSequence Subtitles;
    public Text[] SubtitleTextViews;

    private string currentSubtitleText;

    private IEnumerator Start()
    {
        return this.PlayIntroAudioClip();
    }

    private IEnumerator PlayIntroAudioClip()
    {
        var audio = this.GetComponent<AudioSource>();
        if (audio == null)
        {
            audio = this.gameObject.AddComponent<AudioSource>();
        }

        if (this.IntroAudioClip != null)
        {
            // Start audio clip
            audio.clip = this.IntroAudioClip;
            audio.loop = false;
            audio.Play();

            var subtitles = this.Subtitles.AsQueue();
            var currentSubtitle = subtitles.Dequeue();

            // Wait for audio clip to finish playing
            while (audio.isPlaying)
            {
                float currentTime = audio.time;

                // Skip to next subtitle if necessary
                while (currentSubtitle != null && currentSubtitle.EndTime < currentTime)
                    currentSubtitle = subtitles.Dequeue();

                if (currentSubtitle != null && currentSubtitle.StartTime <= currentTime)
                {
                    this.SetSubtitleText(currentSubtitle.Text);
                }
                else
                {
                    this.SetSubtitleText("");
                }

                yield return null;
            }

            this.SetSubtitleText("");
        }
        else
        {
            Debug.LogError("No audio clip has been specified to play during intro stage");
        }
    }

    private void SetSubtitleText(string text)
    {
        if (this.currentSubtitleText != text)
        {
            this.currentSubtitleText = text;
            for (int i = 0; i < this.SubtitleTextViews.Length; i++)
            {
                this.SubtitleTextViews[i].text = text;
            }
        }

    }

}
